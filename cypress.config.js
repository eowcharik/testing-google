const {defineConfig} = require("cypress");

module.exports = defineConfig({
  e2e: {
    chromeWebSecurity: false,
    baseUrl: "https://www.google.com",
    defaultCommandTimeout: 15000,
    requestTimeout: 10000,
    setupNodeEvents(on, config) {
      // implement node event listeners here
    }
  }
});
