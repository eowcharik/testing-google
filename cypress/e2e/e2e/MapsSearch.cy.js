import MapsPage from "../pages/maps.page";

describe("Search field testing", () => {
  const testLocation = "Paris";

  beforeEach(() => {
    MapsPage.open();
    MapsPage.waitForPageDisplayed();
  });

  it("check search using enter", () => {
    MapsPage.typeInSearchField(testLocation + "{enter}");
    MapsPage.assertThatLocationIsShown();
  });

  it("check search using button", () => {
    MapsPage.typeInSearchField(testLocation);
    MapsPage.clickSearchButton();
    MapsPage.assertThatLocationIsShown();
  });

  it("check clearing search using cross", () => {
    MapsPage.typeInSearchField(testLocation, true);
    MapsPage.clickSearchButton();
    MapsPage.assertThatLocationIsShown();
    MapsPage.clickClearSearchButton();
    cy.contains(MapsPage.pageElements.searchInput, testLocation).should(
      "not.exist"
    );
  });
});

describe("Search functionality testing", () => {
  beforeEach(() => {
    MapsPage.open();
    MapsPage.waitForPageDisplayed();
  });

  context("Success Flows", () => {
    context("searching of locations", () => {
      it("direct quote lead to location + start navigation", () => {
        const testLocation = "Paris";
        const coordinateX = "48.8";

        MapsPage.typeInSearchField(testLocation, true);
        MapsPage.assertThatLocationIsShown();
        MapsPage.assertThatLocationNameContains(testLocation);

        cy.url().should("include", coordinateX);

        //check redirect to navigation + assert destination
        MapsPage.clickStartNavButton();
        MapsPage.assertThatNavigationIsShown();
        MapsPage.assertDestination(testLocation);
      });

      const multiLangStrings = ["Paris", "Париж", "Paryż"];
      multiLangStrings.forEach(lang => {
        it(`Multi language search of same place - search quote ${lang}`, () => {
          const testLocation = "Paris";
          const coordinateX = "48.8";

          MapsPage.typeInSearchField(lang, true);
          MapsPage.assertThatLocationIsShown();
          MapsPage.assertThatLocationNameContains(testLocation);
          cy.url().should("include", coordinateX);
        });
      });

      it("indirect quote lead to live search results (suggestions)", () => {
        const testLocation = "san franc";
        MapsPage.typeInSearchField(testLocation);
        MapsPage.assertThatLiveSearchResultsIsShown();
        MapsPage.assertLiveSearchResults(testLocation);
      });
    });

    context("searching for base activities and services", () => {
      const basicQuotes = ["Coffee", "ATM", "gas station"];
      basicQuotes.forEach(quote => {
        it(`quote:  ${quote}`, () => {
          MapsPage.typeInSearchField(quote, true);
          MapsPage.assertThatSearchResultsIsShown();
        });
      });

      it("indirect quote lead to live search results (suggestions)", () => {
        const testQuery = "gas stat";
        MapsPage.typeInSearchField(testQuery);
        MapsPage.assertThatLiveSearchResultsIsShown();
        MapsPage.assertLiveSearchResults(testQuery);
      });
    });
  });

  context("Negative Flows", () => {
    it("check No results page", () => {
      const quote = "123SDADAS123333&^%^&*(";
      MapsPage.typeInSearchField(quote, true);
      cy.contains(MapsPage.textStrings.searchError(quote));
      cy.contains(MapsPage.textStrings.searchErrorHelper);
    });
  });
});
