class Helpers {
  acceptCookies() {
    const acceptAllButton = 'button[jsname="b3VHJd"]';
    cy.get(acceptAllButton).first().click();
  }
}
export default new Helpers();
