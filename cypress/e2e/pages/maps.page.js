import BasePage from "./base.page";
import Helpers from "./helpers";

class MapsPage extends BasePage {
  baseURL = "/maps/@52.211197,21.0253058,12z?hl=en&entry=ttu";

  textStrings = {
    searchError: quote => {
      return `Google Maps can't find ${quote}`;
    },
    searchErrorHelper:
      "Make sure your search is spelled correctly. Try adding a city, state, or zip code."
  };

  pageElements = {
    contentContainer: '[class="id-content-container"]',
    searchInput: 'input[id="searchboxinput"]',
    searchButton: 'button[id="searchbox-searchbutton"]',
    clearSearch: '[aria-label="Clear search"]',
    liveSearchResults: 'div[jsinstance="*0"]',
    searchResults: '[jsaction*="clickmod:pane"]', // [role="article"]
    searchResultsDirections: '[data-value="Directions"]',
    location: {
      headline: 'h1[class*="fontHeadlineLarge"]',
      startNavButton: 'button[jsaction*="placeActions.directions"]',
      highlightPlaces: '[jsaction*="pane.carousel.photo"]'
    },
    navigation: {
      navigationTopContainer: '[jsnamespace="directions"]',
      destinationInput: destination => {
        return `[aria-label*="Destination ${destination}"]`;
      },
      travelModes: '[jsaction="toggleTravelMode;keydown:keydownTravelMode"]'
    }
  };

  open() {
    cy.visit(this.baseURL);
    Helpers.acceptCookies();
  }

  waitForPageDisplayed() {
    cy.get(this.pageElements.contentContainer).should("be.visible");
    cy.get(this.pageElements.searchInput).should("be.visible");
  }

  clickSearchButton() {
    cy.get(this.pageElements.searchButton).click({force: true});
  }

  clickClearSearchButton() {
    cy.get(this.pageElements.clearSearch).first().click({force: true});
  }

  clickStartNavButton() {
    cy.get(this.pageElements.location.startNavButton).click({force: true});
  }

  assertThatSearchResultsIsShown() {
    cy.get(this.pageElements.searchResults).should("be.visible");
  }

  assertThatLiveSearchResultsIsShown() {
    cy.get(this.pageElements.liveSearchResults).should("be.visible");
  }

  assertLiveSearchResults(assertionText) {
    cy.get(this.pageElements.liveSearchResults).each(result => {
      const textResult = result.text().toLowerCase();
      textResult.includes(assertionText.toLowerCase());
    });
  }

  assertThatLocationIsShown() {
    cy.get(this.pageElements.location.headline).should("be.visible");
    cy.get(this.pageElements.location.highlightPlaces).should("be.visible");
    this.assertThatStartNavButtonIsPresent();
  }

  assertThatStartNavButtonIsPresent() {
    cy.get(this.pageElements.location.startNavButton).should("be.visible");
  }

  assertThatLocationNameContains(text) {
    cy.get(this.pageElements.location.headline).contains(text);
  }

  assertThatNavigationIsShown() {
    cy.get(this.pageElements.navigation.navigationTopContainer).should(
      "be.visible"
    );
    cy.get(this.pageElements.navigation.travelModes).should("be.visible");
  }

  assertDestination(text) {
    cy.get(this.pageElements.navigation.destinationInput(text));
  }

  typeInSearchField(quote, pressSearch = false) {
    cy.get(this.pageElements.searchInput).type(quote, {delay: 80});
    if (pressSearch) {
      this.clickSearchButton();
    }
  }
}
export default new MapsPage();
