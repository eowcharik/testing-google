## Getting started

## Installation Steps:

- Clone the repository.
- Make sure you have Node.js and npm installed on your machine.
- Open a terminal and navigate to the project directory.
- Run `npm install` to install the project dependencies.

## Running Tests

To run the tests, use the following command:

`npm run cypress:open`

This will execute the Cypress test runner and open the Cypress GUI. From there,
you can select and run entire test suite.

## Writing Tests

All test files are located in the **cypress/e2e** directory.

You can create new test files or modify existing ones to add your test cases.
