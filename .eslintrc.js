module.exports = {
  env: {
    browser: true,
    es2021: true,
    node: true
  },
  extends: ["prettier", "plugin:cypress/recommended"],
  plugins: ["cypress"],
  parserOptions: {
    ecmaVersion: "latest",
    sourceType: "module"
  },
  rules: {
    "cypress/no-unnecessary-waiting": 0
  }
};
